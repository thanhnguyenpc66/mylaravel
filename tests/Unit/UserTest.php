<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
        public function testPassLogin()
    {
        $plainPassword ='123';
        $user = new User(
            [
                'name'=>'test',
                'password'=>Hash::make($plainPassword),
                'email'=>'thanh@gmail.com',
                'role'=>'1'
            ]);
        $response = $this->post('/', ['name'=>$user->name,'password'=> $plainPassword]);
        $response = $this->get('frontend/index');
        $response->assertStatus(405);
        // $response->assertRedirect('/frontend/index');
        // $response->assertStatus(302);
    }

    public function testFailLogin()
    {
        $plainPassword ='123';
        $user = new User(
            [
                'name'=>'test',
                'password'=>Hash::make($plainPassword),
                'email'=>'thanh@gmail.com',
                'role'=>'1'
            ]);
        $response = $this->post('/', ['name'=>$user->name,'password'=> '123456']);
        $response = $this->get('/');
        $response->assertStatus(200);
        // $response->assertRedirect('/');
        // $response->assertStatus(302);
    }
}


function save(){
    let hovaten = document.getElementById('hovaten').value;
    let email = document.getElementById('email').value;
    let sdt = document.getElementById('sdt').value;
    let diachi = document.getElementById('diachi').value;
    let gioitinh = '';
    if(document.getElementById('nam').checked){
         gioitinh = document.getElementById('nam').value;
    } else if (document.getElementById('nu').checked){
         gioitinh = document.getElementById('nu').value;
    }

    if(hovaten && email && sdt && diachi && gioitinh){
        let students = localStorage.getItem('students') ? JSON.parse(localStorage.getItem('students')) : [];   
        students.push({
            hovaten: hovaten,
            email: email,
            sdt: sdt,
            diachi: diachi,
            gioitinh: gioitinh,
        });
        localStorage.setItem('students', JSON.stringify(students));
        this.renderLishStudent();
        document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    }
};
function renderLishStudent(){
    let students = localStorage.getItem('students') ? JSON.parse(localStorage.getItem('students')) : [];
    if(students.length === 0){
        document.getElementById('banglocal').style.display = 'none';
        return false;
    }
    document.getElementById('banglocal').style.display = 'block';
    let tableContent = "<tr><th>Họ và Tên</th><th>Địa Chỉ Email</th><th>Số Điện Thoại</th><th>Giới Tính</th><th>Hành Động</th></tr>";
    students.forEach((students,index) => {
        index++;
        let genderLabel = parseInt(students.gioitinh) === 1 ? 'Nữ' : 'Nam';
         tableContent += `<tbody><tr><td>${students.hovaten}</td><td>${students.diachi}</td><td>${students.sdt}</td><td>${genderLabel}</td><td><a href=''><button data-toggle='tooltip' title='Edit'class='pd-setting-ed'><i class='fas fa-edit' aria-hidden='true' style='font-size: 18px;'></i></button></a><a href=''><button data-toggle='tooltip'  title='Trash' class='pd-setting-ed'><i class='fas fa-trash-alt' aria-hidden='true' style='font-size:18px;'></i></button></a></td></tr></tbody>`;
    });
    document.getElementById('tbllocal').innerHTML = tableContent;
};
function save_session(){
    let hovaten = document.getElementById('hovaten').value;  
    let email = document.getElementById('email').value;
    let sdt = document.getElementById('sdt').value;
    let diachi = document.getElementById('diachi').value;
    let gioitinh = '';
    if(document.getElementById('nam').checked){
         gioitinh = document.getElementById('nam').value;
    } else if (document.getElementById('nu').checked){
         gioitinh = document.getElementById('nu').value;
    }

    if(hovaten && email && sdt && diachi && gioitinh){
        let students = sessionStorage.getItem('students') ? JSON.parse(sessionStorage.getItem('students')) : [];   
        students.push({
            hovaten: hovaten,
            email: email,
            sdt: sdt,
            diachi: diachi,
            gioitinh: gioitinh,
        });
        sessionStorage.setItem('students', JSON.stringify(students));
        this.renderLishStudent_session();
        
    }
};
function renderLishStudent_session(){
    let students = sessionStorage.getItem('students') ? JSON.parse(sessionStorage.getItem('students')) : [];
    if(students.length === 0){
        document.getElementById('bangsession').style.display = 'none';
        return false;
    }
    document.getElementById('bangsession').style.display = 'block';
    let tableContent = "<tr><th>Họ và Tên</th><th>Địa Chỉ Email</th><th>Số Điện Thoại</th><th>Giới Tính</th><th>Hành Động</th></tr>";
    students.forEach((students,index) => {
        index++;
        let genderLabel = parseInt(students.gioitinh) === 1 ? 'Nữ' : 'Nam';
         tableContent += `<tbody><tr><td>${students.hovaten}</td><td>${students.diachi}</td><td>${students.sdt}</td><td>${genderLabel}</td><td><a href=''><button data-toggle='tooltip' title='Edit'class='pd-setting-ed'><i class='fas fa-edit' aria-hidden='true' style='font-size: 18px;'></i></button></a><a href=''><button data-toggle='tooltip'  title='Trash' class='pd-setting-ed'><i class='fas fa-trash-alt' aria-hidden='true' style='font-size:18px;'></i></button></a></td></tr></tbody>`;
    });
    document.getElementById('tblsession').innerHTML = tableContent;
};
function setcookie(){
    let hovaten = document.getElementById('hovaten').value;
    let email = document.getElementById('email').value;
    let sdt = document.getElementById('sdt').value;
    let diachi = document.getElementById('diachi').value;
    let gioitinh = '';
    if(document.getElementById('nam').checked){
         gioitinh = document.getElementById('nam').value;
    } else if (document.getElementById('nu').checked){
         gioitinh = document.getElementById('nu').value;
    }
    if(hovaten!="" && email!="" && sdt!="" && diachi!="" && gioitinh!=""){
        document.cookie = "hovaten=" + hovaten + email + sdt + diachi + gioitinh;
        alert("Thành Công");
    } else{
        alert("Fail");
    }
}
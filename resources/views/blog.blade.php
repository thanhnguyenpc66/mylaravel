<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Blog</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<script src="{{asset('js/popper.min.js')}}"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  	<script src="{{asset('js/bootstrap.min.js')}}"></script>
  	<link rel="stylesheet" href="{{asset('css/style-blog.css')}}">
</head>
<body>
	<div class="container">
		<nav class="container navbar navbar-expand-sm">
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
				<a href="index.jsp" class="logo"> <img
					src="images/icons/logo.png" alt="IMG-LOGO">
				</a>
                <!--Menu-->
                <ul class="navbar-nav ml-5">
                    <li class="nav-item pr-5 pl-5">
                    	<a href="index.html"><i class="fa fa-home"></i> Home</a>
                    </li>
                    <li class="nav-item pr-5">
                    	<a href="product.html"><i class='fas fa-list-ul'></i> Shop</a>
                    </li>
                    <li class="nav-item pr-5">
                    	<a href="#"><i class='fas fa-phone-alt'></i> Features</a>
                    </li>
                    <li class="nav-item pr-5">
                    	<a href="blog.html"><i class='fas fa-info-circle'></i> Blog</a>
                    </li>
                    <li class="nav-item"> 
                    	<a href="#"><i class='fas fa-envelope'></i> About</a>
                    </li>
                </ul>
			</div> 
            <ul class="navbar-nav float-right">
				<li class="nav-item mr-5">
		    		<a href="#">Login</a>
		  		</li>
		  		<li class="nav-item">
		    		<a href="#">Register</a>
		  		</li>
			</ul>
		</nav>
		<!-- Đóng menu	 -->
		<section class="background" style="background-image: url(images/heading-pages-05.jpg);">
			<h2 class="text-center title">
				Blog
			</h2>
		</section>
		<!-- Đóng slide		 -->
		<div class="row mt-5">
			<div class="col-sm-8">
				<div class="blog-1">
					<img class="img-blog" src="images/blog-04.jpg" alt="IMG-LOGO">
					<span>16 Dec, 2018</span>
					<h4>Black Friday Guide: Best Sales & Discount Codes</h4>
					<span>By Admin | Cooking, Food | 8 Comments</span>
					<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra,
						 per inceptos himenaeos. Fusce eget dictum tortor. Donec dictum vitae sapien eu varius</p>
					<a href="#" class="btn btn-sm btn-info">Continue Reading ...</a>
				</div>
				<div class="blog-1 mt-5">
					<img class="img-blog" src="images/blog-08.jpg" alt="IMG-LOGO">
					<span>16 Dec, 2018</span>
					<h4>Black Friday Guide: Best Sales & Discount Codes</h4>
					<span>By Admin | Cooking, Food | 8 Comments</span>
					<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra,
						 per inceptos himenaeos. Fusce eget dictum tortor. Donec dictum vitae sapien eu varius</p>
					<a href="#" class="btn btn-sm btn-info">Continue Reading ...</a>
				</div>
				<div class="blog-1 mt-5">
					<img class="img-blog" src="images/blog-05.jpg" alt="IMG-LOGO">
					<span>16 Dec, 2018</span>
					<h4>Black Friday Guide: Best Sales & Discount Codes</h4>
					<span>By Admin | Cooking, Food | 8 Comments</span>
					<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra,
						 per inceptos himenaeos. Fusce eget dictum tortor. Donec dictum vitae sapien eu varius</p>
					<a href="#" class="btn btn-sm btn-info">Continue Reading ...</a>
				</div>
				<div class="blog-1 mt-5">
					<img class="img-blog" src="images/blog-02.jpg" alt="IMG-LOGO">
					<span>16 Dec, 2018</span>
					<h4>Black Friday Guide: Best Sales & Discount Codes</h4>
					<span>By Admin | Cooking, Food | 8 Comments</span>
					<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra,
						 per inceptos himenaeos. Fusce eget dictum tortor. Donec dictum vitae sapien eu varius</p>
					<a href="#" class="btn btn-sm btn-info">Continue Reading ...</a>
				</div>
				<div class="blog-1 mt-5">
					<img class="img-blog" src="images/blog-03.jpg" alt="IMG-LOGO">
					<span>16 Dec, 2018</span>
					<h4>Black Friday Guide: Best Sales & Discount Codes</h4>
					<span>By Admin | Cooking, Food | 8 Comments</span>
					<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra,
						 per inceptos himenaeos. Fusce eget dictum tortor. Donec dictum vitae sapien eu varius</p>
					<a href="#" class="btn btn-sm btn-info">Continue Reading ...</a>
				</div>				
			</div>
			<div class="col-sm-4">
				<form method="post">
					<input type="" name="" class="form-control" placeholder="Search">
				</form>
				<!-- Danh mục -->
				<div class="card mt-3">
					<div class="card-header text-uppercase">
						<strong style="font-size: 20px;">  Categories</strong>
					</div>
					<div class="list-group">
						<a href="#" class="list-group-item list-group-item-action">Fashion</a>
						<a href="#" class="list-group-item list-group-item-action">Beauty</a>
						<a href="#" class="list-group-item list-group-item-action">Street Style</a>
						<a href="#" class="list-group-item list-group-item-action">Life Style</a>
						<a href="#" class="list-group-item list-group-item-action">DIY & Crafts</a>
					</div>
				</div>
				<div class="mt-3">
					<div class="card-header text-uppercase">
						<strong style="font-size: 20px;">  Featured Products</strong>
					</div>
					<div class="mt-3">
						<ul>
							<li class="products">
								<a href="#">
									<img src="images/item-16.jpg" alt="IMG-PRODUCT" class="img-product float-left">
									<div class="float-left ml-3">								
										<span class="float-left">White Shirt</span>							
										<p>$19.00</p>						
									</div>
								</a>
							</li>
							<li class="products">
								<a href="#">
									<img src="images/item-05.jpg" alt="IMG-PRODUCT" class="img-product float-left">
									<div class="float-left ml-3">								
										<span class="float-left">Nixon Porter</span>							
										<p>$39.00</p>						
									</div>
								</a>
							</li>
							<li class="products">
								<a href="#">
									<img src="images/item-17.jpg" alt="IMG-PRODUCT" class="img-product float-left">
									<div class="float-left ml-3">								
										<span class="float-left">Converse All Star</span>							
										<p>$15.00</p>						
									</div>
								</a>
							</li>
							<li class="products">
								<a href="#">
									<img src="images/item-08.jpg" alt="IMG-PRODUCT" class="img-product float-left">
									<div class="float-left ml-3">								
										<span class="float-left">Nixon Porter</span>							
										<p>$16.00</p>						
									</div>
								</a>
							</li>
							<li class="products">
								<a href="#">
									<img src="images/item-03.jpg" alt="IMG-PRODUCT" class="img-product float-left">
									<div class="float-left ml-3">								
										<span class="float-left">Denim jacket blue</span>							
										<p>$20.00</p>						
									</div>
								</a>
							</li>
						</ul>
						
												
					</div>
				</div>
			</div>
		</div>
		<!-- Đóng nội dung chính -->
	<div class="clearfix"></div>
	<footer>
		<div class="container mt-3">
			<div class="row">
				<div class="col-sm-4 mt-3">
					<span class="title">GET IN TOUCH</span>
					<p>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</p>
					<div class="icon">
						<a href="#"><i class="fa fa-skype">ss</i></a>
						
					</div>
				</div>
				<div class="col-sm-8 mt-3">
					<div class="row">
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-4">
									<span class="title">CATEGORIES</span>
									<ul class="danhsach">
										<li><a href="">Man</a></li>
										<li><a href="">Women</a></li>
										<li><a href="">Dresses</a></li>
										<li><a href="">Sunglasses</a></li>
									</ul>							
								</div>						
								<div class="col-sm-4">
									<span class="title">LINKS</span>
									<ul class="danhsach">
										<li><a href="">Search</a></li>
										<li><a href="">About Us</a></li>
										<li><a href="">Contact Us</a></li>
										<li><a href="">Returns</a></li>
									</ul>	
								</div>
								<div class="col-sm-4 danhsachh">
									<span class="title">HELP</span>
									<ul class="danhsach">
										<li><a href="">Track Order</a></li>
										<li><a href="">Returns</a></li>
										<li><a href="">Shipping</a></li>
										<li><a href="">FAQs</a></li>
									</ul>	
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<span class="title">NEWSLETTER</span>
							<div>
								<input type="text" class="mt-2" placeholder=" Email...">
								<button class="boder bg-danger mt-2">SUBSCRIBE</button>
							</div>
							
						</div>
					</div>
				</div>				
			</div>
		</div>				
	</footer>
</body>

</html>
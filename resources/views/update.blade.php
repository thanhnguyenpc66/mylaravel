<!DOCTYPE html>
<html lang="en">
<head>
  <title>Users</title>
  <script src='https://kit.fontawesome.com/yourcode.js'></script>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <div class="row">
    <div class="col-sm-6 mt-3">
          <h3>Add ProDuct</h3><hr>
          <form action="{{URL::to('backend/updatep/'.$product->id)}}" method="post" enctype="multipart/form-data">
          @csrf
            <input class="form-control" type="hidden" value ="{{ $product->id }}" name="id" />
            <div class="form-group">
              <label>NameProduct</label>
              <input class="form-control" value ="{{ $product->nameproduct }}" name="nameproduct" />
            </div>
            <div class="form-group">
              <label>Amount</label>
              <input class="form-control" value="{{ $product->amount }}" name="amount" />
            </div>            
            <div class="form-group">
              <label for="myfile">Images</label>
              <input type="file" id="myfile" value="fff" name="images">
              <img src="{{URL::to('images/'.$product->images)}}" alt="" style= "width:120px;height:auto;" >
            </div><hr>
            <div class="form-group">
              <a href=""><button class="btn btn-default border" type="submid" name="sbm">Update</button></a>
            </div>
          </form>
        </div>
  </div>
</body>
</html>
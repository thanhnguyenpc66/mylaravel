<!DOCTYPE html>
<html>
<head>
	<title>LOGIN</title>
    <link rel="stylesheet" href="{{asset('css/login.css')}}">
</head>
<body>
<form action="{{ route('sendmail') }}" method="post">
    @method('post')
    @csrf
  <div class="imgcontainer">
    <h1>SENDMAIL</h1>
  </div>

  <div class="container">
    <label for="to"><b>Người Nhận</b></label>
    <input type="text" placeholder="Nhập Người Nhận" name="nguoinhan" required>
    <label for="tieude"><b>Tiêu Đề</b></label>
    <input type="text" placeholder="Nhập Tiêu Đề" name="tieude" required>
    <label for="noidung"><b>Nội Dung</b></label><br/>
    <textarea rows="9" cols="130" name="noidung" placeholder="Nhập Nội Dung"> 
    </textarea>

    <button type="submit">GỬI</button>
  </div>

</form>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>
    <link rel="stylesheet" href="{{asset('css/backend.css')}}">
</head>
<body>
    <div class="top"></div>
    <div class="left">
		<ul>
			<li><a href="{{ route('backend.dashboard.index')}}">Dashboard</a></li>
			<li><a href="{{ route('backend.product.index')}}">Product</a></li>
			<li><a href="{{ route('backend.user.index')}}">User</a></li>
		</ul>
	</div>
	<div class="right"></div>
	<div class="footer"></div>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Product</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<script src="{{asset('js/popper.min.js')}}"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<link rel="stylesheet" href="{{asset('css/style-product.css')}}">
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
</head>
<body ng-app="myapp">
	<div class="container">
		<nav class="container navbar navbar-expand-sm">
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
				<a href="#" class="logo"> <img
					src="{{asset('images/icons/logo.png')}}" alt="IMG-LOGO">
				</a>
                <!--Menu-->
                <ul class="navbar-nav ml-5">
                    <li class="nav-item pr-5 pl-5">
                    	<a href="#"><i class="fa fa-home"></i> Home</a>
                    </li>
                    <li class="nav-item pr-5">
                    	<a href="#"><i class='fas fa-list-ul'></i> Shop</a>
                    </li>
                    <li class="nav-item pr-5">
                    	<a href="#"><i class='fas fa-phone-alt'></i> Features</a>
                    </li>
                    <li class="nav-item pr-5">
                    	<a href="#"><i class='fas fa-info-circle'></i> Blog</a>
                    </li>
                    <li class="nav-item"> 
                    	<a href="#"><i class='fas fa-envelope'></i> About</a>
                    </li>
                </ul>
			</div> 
            <ul class="navbar-nav float-right">
				<li class="nav-item mr-5">
		    		<a href="#">Login</a>
		  		</li>
		  		<li class="nav-item">
		    		<a href="#">Register</a>
		  		</li>
			</ul>
		</nav>
		<div class="row mt-5">
			<div class="col-sm-3">
				<div class="card mt-3">
					<div class="card-body">
						<form method="post">
							<input type="" name="" class="form-control" placeholder="Keywords">
						</form>
					</div>
				</div>						
				<!-- Danh mục -->
				<div id="accordion">
					<div class="card mt-2">
						<div class="card-header border-bottom-0">
							<div class="collapsed card-link cursor" data-toggle="collapse" href="#collapseThree">
								<i class="fa fa-star" aria-hidden="true"></i>
								<strong>Categories</strong>
							</div>
						</div>
						<div class="list-group">
							<a href="" class="list-group-item list-group-item-action">All</a>
							<a href="" class="list-group-item list-group-item-action">Man</a>
							<a href="" class="list-group-item list-group-item-action">Women</a>
							<a href="" class="list-group-item list-group-item-action">Kids</a>
						</div>
					</div>
					<div class="card mt-2">
						<div class="card-header border-bottom-0">
							<div class="cursor" data-toggle="collapse" href="#collapseOne">
								<i class="fa fa-list" aria-hidden="true"></i>
								<strong> Other</strong>
							</div>
						</div>
						<div id="collapseOne" class="collapse" data-parent="#accordion">
							<div class="list-group-flush">
								<a href="" class="list-group-item list-group-item-action">T-shirt <span class="badge badge-warning text-white float-right">New</span></a>
								<a href="" class="list-group-item list-group-item-action">Jeans </a>
								<a href="" class="list-group-item list-group-item-action">Backpack </a>
								<a href="" class="list-group-item list-group-item-action">Watch <span class="badge badge-danger float-right">Sell 5%</span></a>
								<a href="" class="list-group-item list-group-item-action">Shoes</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Sản phẩm -->
			<div class="col-sm-9">
				<div ng-controller ="myctrl" class="row">
					<div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 pb-3" ng-repeat="k in products">				
						<div class="product">
							<a href="#">
								<img src="{{asset('images/<%k.images%>')}}" style="height: 250px; max-width: 100%;" class="img-product img-thumbnail" alt="">
								<span class="ten"><%k.name%></span><br>
								<span class="gia"><%k.price%></span>
							</a>
						</div>							
					</div>
				</div>								
			</div>
		</div>
		<!-- Đóng nội dung chính -->
	<div class="clearfix"></div>
	<footer>
		<div class="container mt-3">
			<div class="row">
				<div class="col-sm-4 mt-3">
					<span class="title">GET IN TOUCH</span>
					<p>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</p>
					<div class="icon">
						<a href="#"><i class="fa fa-skype">ss</i></a>
						
					</div>
				</div>
				<div class="col-sm-8 mt-3">
					<div class="row">
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-4">
									<span class="title">CATEGORIES</span>
									<ul class="danhsach">
										<li><a href="">Man</a></li>
										<li><a href="">Women</a></li>
										<li><a href="">Dresses</a></li>
										<li><a href="">Sunglasses</a></li>
									</ul>							
								</div>						
								<div class="col-sm-4">
									<span class="title">LINKS</span>
									<ul class="danhsach">
										<li><a href="">Search</a></li>
										<li><a href="">About Us</a></li>
										<li><a href="">Contact Us</a></li>
										<li><a href="">Returns</a></li>
									</ul>	
								</div>
								<div class="col-sm-4 danhsachh">
									<span class="title">HELP</span>
									<ul class="danhsach">
										<li><a href="">Track Order</a></li>
										<li><a href="">Returns</a></li>
										<li><a href="">Shipping</a></li>
										<li><a href="">FAQs</a></li>
									</ul>	
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<span class="title">NEWSLETTER</span>
							<div>
								<input type="text" class="mt-2" placeholder=" Email...">
								<button class="boder bg-danger mt-2">SUBSCRIBE</button>
							</div>
							
						</div>
					</div>
				</div>				
			</div>
		</div>				
	</footer>
</body>
<script src="{{ asset('js/data.js') }}"></script>
<script src="{{ asset('js/product.js') }}"></script>
</html>
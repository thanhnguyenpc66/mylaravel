<!DOCTYPE html>
<html lang="en">
<head>
  <title>STUDENT</title>
  <script src='https://kit.fontawesome.com/yourcode.js'></script>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body onload="renderLishStudent(),renderLishStudent_session()">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 mt-3">
            <h3>Sử Dụng LocalStorage, SessionStorage, Cookie</h3><hr>
            <form method="post">
            @csrf
              <div class="form-group">
                <label>Họ và tên</label>
                <input class="form-control" id="hovaten" name="hovaten" />
              </div>
              <div class="form-group">
                <label>Địa chỉ email</label>
                <input class="form-control" id="email" name="email" />
              </div>            
              <div class="form-group">
                <label>Số điện thoại</label>
                <input class="form-control"  name="sdt" id="sdt" />
              </div>
              <div class="form-group">
                <label>Địa chỉ</label>
                <input class="form-control" id="diachi" name="diachi" />
              </div> <div class="form-group">
                <label>Giới tính</label><br>
                <input type="radio" id="nam" name="gioitinh" value="Nam">
                <label for="nam">Nam</label><br>
                <input type="radio" id="nu" name="gioitinh" value="Nữ">
                <label for="nu">Nữ</label><br>
              </div> 
              <hr>
              <div class="form-group">
                <button class="btn btn-default border" onclick="save()" >THÊM LOCAL</button>
                <button class="btn btn-default border" onclick="save_session()" >THÊM SESSION</button>
                <button class="btn btn-default border" onclick="setcookie()" >SET COOKIE</button>
              </div>
            </form>
          </div>
    </div>
    <div class="row" id="banglocal">
      <h2>Danh Sách LocalStorage</h2>
      <table class="table table-condensed" id="tbllocal">
        
      </table>
    </div>
    <div class="row" id="bangsession">
      <h2>Danh Sách SessionStorage</h2>
      <table class="table table-condensed" id="tblsession">
        
      </table>
    </div>
    </div>
  <script src="{{asset('js/student.js')}}"></script>
</body>
</html>
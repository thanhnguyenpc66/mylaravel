<!DOCTYPE html>
<html>
<head>
	<title>LOGIN</title>
    <link rel="stylesheet" href="{{asset('css/login.css')}}">
</head>
<body>
<form action="{{ route('frontend.index') }}" method="post">
    @method('post')
    @csrf
  <div class="imgcontainer">
    <h1>{{__('LOGIN')}}</h1>
  </div>
  <a href="{{route('language.index',['vi'])}}" class="btnx">Tiếng Việt</a>
  <a href="{{route('language.index',['en'])}}" class="btnx">English</a>
  <div class="container">
    <label for="name"><b>{{__('Username')}}</b></label>
    <input type="text" placeholder="Enter Username" name="name" required>

    <label for="pass"><b>{{__('Password')}}</b></label>
    <input type="password" placeholder="Enter Password" name="pass" required>

    <button type="submit">{{__('LOGIN')}}</button>
    <a href="{{route('register')}}" class="btn btn-success">
    <button type="button" class="btn btn-success">{{__('Register')}}</button></a>
    <label>
      <input type="checkbox" checked="checked" name="remember">{{__('Remember me')}}
    </label>
  </div>
  <div class="container" style="background-color:#f1f1f1">
    <button type="button" class="cancelbtn">{{__('Cancel')}}</button>
    <span class="psw">Forgot <a href="#">password?</a></span>
  </div>
  <style>
    .btnx{
      margin-left: 10px;
      text-decoration: none;
      text-transform: uppercase;
    }
  </style>
</form>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
	<title>HOME</title>
    <h1>Test Còn Nhiều</h1>
    <?php echo '<h3 style="color:red">' . asset('css/style.css') ?>
</hr><br/>
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('resource/css/style.css') }}"> -->
</head>
<body>
<a href="/user">User</a> |
<a href="/product">Product</a> |
<a href="/name">Name</a> |

</hr>
<ul>
    <li><a href ="/news/1/category/101"> Bài viết số 1</a></li>
    <li><a href ="/news/2/category/102"> Bài viết số 2</a></li>
    <li><a href ="/news/3/category/103"> Bài viết số 3</a></li>
    <li><a href ="/news/4/category/104"> Bài viết số 4</a></li>
    <li><a href ="/news/5/category/105"> Bài viết số 5</a></li>
</ul>

</hr>

<ul>
    <li><a href ="{{ route('user') }}">User</a></li>
    <li><a href ="{{ route('user') }}">User</a></li>
    <li><a href ="{{ route('user') }}">User</a></li>
    <li><a href ="{{ route('user') }}">User</a></li>
    <li><a href ="{{ route('user') }}">User</a></li>
</ul>

</hr>
<h2>Route Name Có Tham Số</h2>
<ul>
    <li><a href ="{{ route('user.show', ['id' => 1]) }}">User by detail</a></li>
    <li><a href ="{{ route('user.show', ['id' => 's']) }}">User by detail</a></li> <!-- Đặt điều kiện id ở app/Provider/RouteService... -->
    <li><a href ="{{ route('user.show', ['id' => 3]) }}">User by detail</a></li>
</ul>
</hr>
<ul>
    <li><a href ="{{ route('user.show.branch', ['id' => 1, 'branchId' => 1]) }}">User by detail</a></li>
    <li><a href ="{{ route('user.show.branch', ['id' => 2, 'branchId' => 2]) }}">User by detail</a></li>
    <li><a href ="{{ route('user.show.branch', ['id' => 3, 'branchId' => 3]) }}">User by detail</a></li>
</ul>
</hr>
<h2>Route Group</h2>
<ul>
    <li><a href ="{{ route('frontend.user') }}">User management</a></li>
    <li><a href ="{{ route('frontend.product') }}">Product management</a></li>
    <li><a href ="{{ route('frontend.category') }}">Category management</a></li>
    <li><a href ="{{ route('frontend.news') }}">News management</a></li>
</ul>
<h2>Route Controller</h2>
<ul>
    <li><a href ="{{ route('backend.user') }}">User </a></li>
    <li><a href ="{{ route('backend.product') }}">Product </a></li>
    <li><a href ="{{ route('backend.category') }}">Category </a></li>
    <li><a href ="{{ route('backend.news') }}">News </a></li>
</ul>
</body>
</html>



<!DOCTYPE html>
<html lang="en">
<head>
  <title>Users</title>
  <script src='https://kit.fontawesome.com/yourcode.js'></script>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <div class="row">
    <div class="col-sm-6 mt-3">
          <h3>Add ProDuct</h3><hr>
          <form action="{{ route('addproduct') }}" method="post" enctype="multipart/form-data">
          @csrf
            <div class="form-group">
              <label>NameProduct</label>

              <input class="form-control" value =""name="nameproduct" />
            </div>
            <div class="form-group">
              <label>Amount</label>
              <input class="form-control" name="amount" />
            </div>            
            <div class="form-group">
              <label for="myfile">Images</label>
              <input type="file" id="myfile" name="images">
            </div><hr>
            <div class="form-group">
              <button class="btn btn-default border" type="submid" name="sbm">ADD</button>
            </div>
            
          </form>
          <form action="{{ route('importProduct') }}" method="post" enctype="multipart/form-data">
          @csrf
            <label for="myfile">Chọn File Import</label>
              <input type="file" name="file" accept=".xlsx">
            <div class="form-group">
              <a href="{{route('importProduct')}}"><button class="btn btn-default border" >Import Excel</button></a>
            </div>
            
          </form>
          <div class="form-group">
              <a href="{{route('exportProduct')}}"><button class="btn btn-default border" >Export Excel</button></a>
            </div>
  </div>
  <div class="row">
    <h2>List User</h2>
    <table class="table table-condensed">
      <thead>
        <tr>
          <th>NameProduct</th>
          <th>Amount</th>
          <th>Images</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $row)
        <tr>
          <td>{{$row->nameproduct}}</td>
          <td>{{$row->amount}}</td>
          <td><img src="{{URL::to('images/'.$row->images)}}" alt="" class="hinh" style="width:120px;height:auto;"></td>
      <td><a href="{{URL::to('backend/update/'.$row->id)}}">
        <button data-toggle="tooltip" title="Edit"
          class="pd-setting-ed">
          <i class="fas fa-edit" aria-hidden="true"
            style="font-size: 18px;"></i>
        </button></a>
        <a href="{{URL::to('backend/delete/'.$row->id)}}">
        <button data-toggle="tooltip" title="Trash"
          class="pd-setting-ed">
          <i class="fas fa-trash-alt" aria-hidden="true"
            style="font-size: 18px;"></i>
        </button></a>
      </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  </div>
  

</body>
</html>
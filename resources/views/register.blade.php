<!DOCTYPE html>
<html>
<head>
	<title>LOGIN</title>
    <link rel="stylesheet" href="{{asset('css/login.css')}}">
</head>
<body>
<form action="{{ route('getregister') }}" method="post">
    @method('post')
    @csrf
  <div class="imgcontainer">
    <h1>REGISTER</h1>
  </div>

  <div class="container">
    <label for="uname"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="name" required>
    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="pass" required>

    <button type="submit">REGISTER</button>
    <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label>
  </div>

  <div class="container" style="background-color:#f1f1f1">
    <button type="button" class="cancelbtn">Cancel</button>    
    <span class="psw">Forgot <a href="#">password?</a></span>
  </div>
</form>
<ul class="navbar-nav float-left">
		  		<li class="nav-item">
		    		<a href="{{ route('frontend.logout') }}">Logout</a>
		  		</li>
			</ul>
</body>
</html>

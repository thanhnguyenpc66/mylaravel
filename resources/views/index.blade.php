<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Home</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<script src="{{asset('js/popper.min.js')}}"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src='https://kit.fontawesome.com/a076d05399.js'></script>
  	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  	<script src="{{asset('js/bootstrap.min.js')}}"></script>
  	<link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>
	<div class="container">
		<nav class="container navbar navbar-expand-sm">
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
				<a href="frontend/index" class="logo"> <img
					src="{{asset('images/icons/logo.png')}}" alt="IMG-LOGO">
				</a>
                <!--Menu-->
                <ul class="navbar-nav ml-5">
                    <li class="nav-item pr-5 pl-5">
                    	<a href="#"><i class="fa fa-home"></i> Home</a>
                    </li>
                    <li class="nav-item pr-5">
                    	<a href="{{ route('frontend.product') }}"><i class='fas fa-list-ul'></i> Product</a>
                    </li>
                    <li class="nav-item pr-5">
                    	<a href="{{ route('frontend.shop') }}"><i class='fas fa-phone-alt'></i> Shop</a>
                    </li>
                    <li class="nav-item pr-5">
                    	<a href="{{ route('thuviencarbon') }}"><i class='fas fa-info-circle'></i> Carbon</a>
                    </li>
                    <li class="nav-item"> 
                    	<a href="{{ route('frontend.student') }}"><i class='fas fa-envelope'></i> LocalStorage</a>
                    </li>
                </ul>
			</div> 
			@if($quyen==1)
            <ul class="navbar-nav float-right">
				<li class="nav-item mr-5">
		    		<a href="{{ route('frontend.login') }}">Login</a>
		  		</li>
		  		<li class="nav-item">
		    		<a href="{{ route('frontend.logout') }}">Logout</a>
		  		</li>
			</ul>
			@endif
			@if($quyen)
			<ul class="navbar-nav float-right">
				<li class="nav-item mr-5">
		    		<a href="#"><h5>Quyền: {{$quyen}}</h5></a>
		  		</li>
			</ul>
			@endif
		</nav>
		<!-- Đóng menu	 -->
		<div id="demo" class="carousel slide mt-3" data-ride="carousel">
			<ul class="carousel-indicators">
				  <li data-target="#demo" data-slide-to="0" class="active"></li>
				  <li data-target="#demo" data-slide-to="1"></li>
				  <li data-target="#demo" data-slide-to="2"></li>
			</ul>
			<div class="carousel-inner">
				  <div class="carousel-item active">
					<img src="{{asset('images/master-slide-02.jpg')}}" alt="Đà Lạt nhìn từ trên cao" >
					<div class="carousel-caption">
					</div>   
				  </div>
				  <div class="carousel-item">
					<img src="{{asset('images/master-slide-03.jpg')}}" alt="Đà Lạt nhìn từ bên phải" >
					<div class="carousel-caption">
					</div>   
				  </div>
				  <div class="carousel-item">
					<img src="{{asset('images/master-slide-04.jpg')}}" alt="Đà Lạt nhìn từ bên trái" >
					<div class="carousel-caption">
					</div>   
				  </div>
			</div>
			<a class="carousel-control-prev" href="#demo" data-slide="prev">
				  <span class="carousel-control-prev-icon"></span>
			</a>
			<a class="carousel-control-next" href="#demo" data-slide="next">
				  <span class="carousel-control-next-icon"></span>
			</a>
		</div>
		<!-- Đóng slide		 -->
		<div class="mt-3">
			<div class="text-center">
				<h2 class="text-uppercase">OUR BLOG</h2>
				<nav class="navbar navbar-expand-sm">
					<ul class="navbar-nav mg">
						<li class="nav-item pr-3">
							<a href="#">Best Seller</a>
						</li>
						<li class="nav-item pr-3">
							<a href="#">Featured</a>
						</li>
						<li class="nav-item pr-3">
							<a href="#">Sale</a>
						</li>
						<li class="nav-item pr-3">
							<a href="#">Top Rate</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="row mt-2">
				<div class="col-sm-3">
					<img src="{{asset('images/item-02.jpg')}}" style="height: auto; max-width: 100%;" alt="">
					<h5>Herschel supply co 25l</h5>
					<p>$75.00</p>
				</div>
				<div class="col-sm-3">
					<img src="{{asset('images/item-08.jpg')}}" style="height: auto; max-width: 100%;" alt="">
					<h5>Herschel supply co 25l</h5>
					<p>$75.00</p>
				</div>
				<div class="col-sm-3">
					<img src="{{asset('images/item-12.jpg')}}" style="height: auto; max-width: 100%;" alt="">
					<h5>Herschel supply co 25l</h5>
					<p>$75.00</p>
				</div>
				<div class="col-sm-3">
					<img src="{{asset('images/item-06.jpg')}}" style="height: auto; max-width: 100%;" alt="">
					<h5>Herschel supply co 25l</h5>
					<p>$75.00</p>
				</div>
				<div class="col-sm-3">
					<img src="{{asset('images/item-03.jpg')}}" style="height: auto; max-width: 100%;" alt="">
					<h5>Herschel supply co 25l</h5>
					<p>$75.00</p>
				</div>
				<div class="col-sm-3">
					<img src="{{asset('images/item-01.jpg')}}" style="height: auto; max-width: 100%;" alt="">
					<h5>Herschel supply co 25l</h5>
					<p>$75.00</p>
				</div>
				<div class="col-sm-3">
					<img src="{{asset('images/item-16.jpg')}}" style="height: auto; max-width: 100%;" alt="">
					<h5>Herschel supply co 25l</h5>
					<p>$75.00</p>
				</div>
				<div class="col-sm-3">
					<img src="{{asset('images/item-11.jpg')}}" style="height: auto; max-width: 100%;" alt="">
					<h5>Herschel supply co 25l</h5>
					<p>$75.00</p>
				</div>
			</div>
		</div>

		<div class="mt-3">
			<div class="text-center">
				<h2 class="text-uppercase">OUR BLOG</h2>
			</div>
			<div class="row">
				<div class=" col-md-4 float-left">
					<a href="#" class="thumbnail">
						<img src="{{asset('images/blog-01.jpg')}}" alt="" class="hinh img-thumbnail">
					</a>
					<div class="caption">
						<h5>Black Friday Guide: Best Sales & Discount Codes</h5>
						<span>By Nancy Ward on July 22, 2017</span>
						<p class="mt-2">Duis ut velit gravida nibh bibendum commodo. Sus-pendisse pellentesque mattis augue id euismod. Inter-dum et malesuada fames</p>
					</div>
				</div>
				<div class=" col-md-4 float-left pd-0">
					<a href="#" class="thumbnail">
						<img src="{{asset('images/blog-02.jpg')}}" alt="" class="hinh img-thumbnail">
					</a>
					
					<div class="caption">
						<h5>The White Sneakers Nearly Every Fashion Girls Own</h5>
						<span>By Nancy Ward on July 18, 2017</span>
						<p class="mt-2">Nullam scelerisque, lacus sed consequat laoreet, dui enim iaculis leo, eu viverra ex nulla in tellus. Nullam nec ornare tellus, ac fringilla lacus. Ut sit ame</p>							
					</div>
				</div>
				<div class=" col-md-4 float-right">
					<a href="#" class="thumbnail">
						<img src="{{asset('images/blog-03.jpg')}}" alt="" class="hinh img-thumbnail">
					</a>
					
					<div class="caption">
						<h5>New York SS 2018 Street Style: Annina Mislin</h5>
						<span>By Nancy Ward on July 2, 2017</span>
						<p class="mt-2">Proin nec vehicula lorem, a efficitur ex. Nam vehicula nulla vel erat tincidunt, sed hendrerit ligula porttitor. Fusce sit amet maximus nunc</p>							
					</div>
				</div>
			</div>
		</div>
		<div class="mt-2">
			<div class="text-center">
				<h2 class="text-uppercase">@ FOLLOW US ON INSTAGRAM</h2>
			</div>
			<div class="col-md-3 float-left">
				<a href="#" class="thumbnail">
					<img src="{{asset('images/gallery-03.jpg')}}" alt="" class="hinh">
				</a>
			</div>	
			<div class="col-md-3 float-left">
				<a href="#" class="thumbnail">
					<img src="{{asset('images/gallery-07.jpg')}}" alt="" class="hinh">
				</a>
			</div>
			<div class="col-md-3 float-left">
				<a href="#" class="thumbnail">
					<img src="{{asset('images/gallery-09.jpg')}}" alt="" class="hinh">
				</a>
			</div>
			<div class="col-md-3 float-right">
				<a href="#" class="thumbnail">
					<img src="{{asset('images/gallery-13.jpg')}}" alt="" class="hinh">
				</a>
			</div>		
		</div>		
	</div>
	<div class="clearfix"></div>
	<footer>
		<div class="container mt-3">
			<div class="row">
				<div class="col-sm-4 mt-3">
					<span class="title">GET IN TOUCH</span>
					<p>Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879</p>
					<div class="icon">
						<a href="#"><i class="fa fa-skype">ss</i></a>
						
					</div>
				</div>
				<div class="col-sm-8 mt-3">
					<div class="row">
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-4">
									<span class="title">CATEGORIES</span>
									<ul class="danhsach">
										<li><a href="">Man</a></li>
										<li><a href="">Women</a></li>
										<li><a href="">Dresses</a></li>
										<li><a href="">Sunglasses</a></li>
									</ul>							
								</div>						
								<div class="col-sm-4">
									<span class="title">LINKS</span>
									<ul class="danhsach">
										<li><a href="">Search</a></li>
										<li><a href="">About Us</a></li>
										<li><a href="">Contact Us</a></li>
										<li><a href="">Returns</a></li>
									</ul>	
								</div>
								<div class="col-sm-4 danhsachh">
									<span class="title">HELP</span>
									<ul class="danhsach">
										<li><a href="">Track Order</a></li>
										<li><a href="">Returns</a></li>
										<li><a href="">Shipping</a></li>
										<li><a href="">FAQs</a></li>
									</ul>	
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<span class="title">NEWSLETTER</span>
							<div>
								<input type="text" class="mt-2" placeholder=" Email...">
								<button class="boder bg-danger mt-2">SUBSCRIBE</button>
							</div>
							
						</div>
					</div>
				</div>				
			</div>
		</div>				
	</footer>
</body>

</html>
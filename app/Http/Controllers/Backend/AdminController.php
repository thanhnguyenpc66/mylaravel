<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = DB::table('product');
        $query = $query->orderBy("id","Desc");
        $query = $query->select("*");
        $date = $query->paginate(15);
        return view('product',$date);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nameproduct = $request->nameproduct;
        $amount = $request->amount;
        $images = $_FILES['images']['name'];

        DB::table('product')->insert([
            'nameproduct'=>$nameproduct,
            'amount'=>$amount, 
            'images'=>$images,
            ]);
            return redirect('frontend/product');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo 'show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('update')->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        // dd($id);
        // $product = Product::find($id);
        // // dd($product);

        // return view('update',$product);
    }
    public function save(Request $request, $id)
    {
        // dd($_FILES['images']['name']);
        $pr = [
            'nameproduct' => $request->nameproduct,
            'amount' => $request->amount,
            'images' => $_FILES['images']['name'],
        ];
         $product = DB::table('product')->where('id',$request->id)->update($pr);

        return redirect('frontend/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $product = Product::all();
        Product::find($id)->delete();
        return redirect('frontend/product');
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

use DB;
class UserController extends Controller
{
    public function index(){
        return view('index');
    }
    public function shop(){
        return view('shop');
    }

    public function product(){
        $query = DB::table('product');
        $query = $query->orderBy("id","Desc");
        $query = $query->select("*");
        $date = $query->paginate(15);
        return view('product',$date);
    }

    public function blog(){
        return view('blog');
    }

    public function test(){
        return view('test');
    }

    public function GetLogin(){
        return view('login');
    }

    public function PostLogin(Request $request){
        $request -> validate([
            'name' => 'required|min:3|max:12',
            'pass' => 'required|min:3|max:12'
        ]);
        $name = $request->name;
        $pass = $request->pass;
        $quyen ='';
        

        if(Auth::attempt(['name'=>$name,'password'=>$pass]))
        {
            if(Auth::check() && Auth::user()->role == 1){ 
                $quyen='Admin';            
                 return view('index')->with('quyen', $quyen);
            } else{ 
                $quyen='User';   
                return view('index')->with('quyen', $quyen);
                //  return redirect()->route('register');
            }
        }else{
             return view('login');
        }
    }

    public function register(){
        return view('register');
    }
    
    public function getregister(Request $request){
        $name = $request->name;
        $email = $request->email;
        $pass = bcrypt($request->pass);
        DB::table('users')->insert(['name'=>$name,'password'=>$pass, 'email'=>$email, 'role' => 1]);
        echo 'Thêm thành công';
    }
    public function logout(){
        Auth::logout();
        return view('/login');
    }
    public function student(){
        return view('student');
    }
}

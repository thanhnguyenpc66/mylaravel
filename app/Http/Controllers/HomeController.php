<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App\Exports\ProductExport;
use App\Imports\UsersImport;
use App\Imports\ProductImport;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function sendMail(){
        $data = [
            'name' => 'Nguyễn Đức Thành',
            'age' => '26',
        ];
        Mail::send('email',$data,function($message){
            $message->from('thanhnguyenpc66@gmail.com','Test');
            $message->to('thanhnguyenpc66@gmail.com', 'Test1');
            $message->subject('Thư Test 2');
        });
    }
    public function carbon(){
        $ngayHienTai = Carbon::now('Asia/Ho_Chi_Minh');
        $namThangNgay = $ngayHienTai->toDateString();
        $gioPhutGiay = $ngayHienTai->toTimeString();
        $ntngpg = $ngayHienTai->toDateTimeString();


        echo 'Ngày Hiện Tại: ', $ngayHienTai,'<br>';
        echo 'Năm Hiện Tại(yyyy/mm/dd) : ', $namThangNgay,'<br>';
        echo 'Giờ Hiện Tại: ', $gioPhutGiay,'<br>';
        echo 'Ngày, Giờ Hiện Tại: ', $ntngpg,'<br>';
        echo 'Ngày: ',Carbon::now()->day,'<br>'; //ngày
        echo 'Tháng: ',Carbon::now()->month,'<br>'; //tháng
        echo 'Năm: ',Carbon::now()->year,'<br>'; //năm
    }
    public function exportUser(){
        return Excel::download(new UsersExport,'users.xlsx');
    }

    public function exportProduct(){
        return Excel::download(new ProductExport,'product.xlsx');
    }
    public function importProduct(Request $request){
        $path = $request->file('file')->getRealPath();
        Excel::import(new ProductImport, $path);
        return back();
    }
    public function sendEMail()
    {
        // return view('sendmail');
    }
    public function Mail(Request $request){
        // $noidung = $request->noidung;
        // $nguoinhan = $request->nguoinhan;
        // $tieude = $request->tieude;
        // $data = [
        //     'name' => 'Nguyễn Đức Thành',
        //     'age' => '26',
        // ];
        // Mail::send('sendmail',$data,function($message){
        //     $message->from('thanhnguyenpc66@gmail.com','Test');
        //     $message->to('$nguoinhan', '$nguoinhan');
        //     $message->subject('$tieude');
        // });
        // return view('sendmail');
    }
}

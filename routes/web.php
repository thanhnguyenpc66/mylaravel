<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

//Route::prefix('Frontend')->group(function(){ CÁCH VIẾT THỨ 2
Route::group(['prefix' => 'frontend','middleware'=>'FrontendRole','namespace'=>'App\Http\Controllers\Backend'], function(){
// Route::prefix('frontend')->namespace('App\Http\Controllers\Backend')->group(function() {
    
    Route::post('index', 'UserController@PostLogin')->name('frontend.index');
    Route::get('product', 'UserController@product')->name('frontend.product');
    Route::get('logout', 'UserController@logout')->name('frontend.logout');
    Route::get('login', 'UserController@GetLogin')->name('frontend.login');
    Route::get('student', 'UserController@student');
    Route::post('student', 'UserController@student')->name('frontend.student');
    Route::get('shop', 'UserController@shop')->name('frontend.shop');
    Route::get('language/{language}', 'LanguageController@index')->name('language.index');
    
});

Route::prefix('backend')->namespace('App\Http\Controllers\Backend')->group(function() {
    Route::get('/', 'AdminController@index');
    Route::post('add', 'AdminController@store')->name('addproduct');
    Route::get('delete/{id}', 'AdminController@destroy')->name('deleteproduct');
    Route::get('update/{id}', 'AdminController@edit')->name('updateproduct');
    Route::post('updatep/{id}', 'AdminController@save')->name('postupdate');
});

Auth::routes();
Route::prefix('re')->namespace('App\Http\Controllers\Backend')->group(function() {
    Route::post('getRegister', 'UserController@getregister')->name('getregister');
    Route::get('register', 'UserController@register')->name('register');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('frontend.login');

Route::prefix('email')->namespace('App\Http\Controllers')->group(function() {
    Route::get('/','HomeController@sendMail'); 
    // Route::get('send','HomeController@sendEMail');  
    // Route::post('send','HomeController@Mail')->name('sendmail');   
});
Route::prefix('thuvien')->namespace('App\Http\Controllers')->group(function() {
    Route::get('/','HomeController@carbon')->name('thuviencarbon');  
});
Route::prefix('excel')->namespace('App\Http\Controllers')->group(function() {
    Route::get('/','HomeController@exportUser');
    Route::get('exportProduct','HomeController@exportProduct')->name('exportProduct'); 
    Route::post('importProduct','HomeController@importProduct')->name('importProduct');  
});


